import axios from "axios";

axios.defaults.baseURL = 'http://backend-api.localhost:8080/';
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');